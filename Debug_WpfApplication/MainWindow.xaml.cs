﻿using System.Windows;
using System.Windows.Media;
using System.ComponentModel;

namespace Debug_WpfApplication
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private int _word;

        public int Word
        {
            get { return _word; }
            set
            {
                _word = value;
                SendPropertyChanged("Word");
            }
        }

        public Brush ColorTrue
        {
            get; set;
        }
        public Brush ColorFalse
        {
            get; set;
        }

        public MainWindow()
        {
            ColorTrue = Brushes.Green;
            ColorFalse = Brushes.Red;
            InitializeComponent();
            DataContext = this;
        }

        void SendPropertyChanged(string property)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
