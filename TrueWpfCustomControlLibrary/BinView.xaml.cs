﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace TrueWpfCustomControlLibrary
{
    public partial class BinView : UserControl, INotifyPropertyChanged
    {
        private BinViewModel _viewModel;

        #region WordValue

        public static readonly DependencyProperty WordValueProperty = DependencyProperty.Register("WordValue",
            typeof(int), typeof(BinView), new PropertyMetadata(0, WordValuePropertyChanged));

        private static void WordValuePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            (sender as BinView).OnWordValuePropertyChanged(sender, e);
        }

        private void OnWordValuePropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            _viewModel.WordValue = WordValue;
        }

        public int WordValue
        {
            get { return (int)GetValue(WordValueProperty); }
            set
            {
                SetValue(WordValueProperty, value);
            }
        }

        #endregion

        #region ColorTrue

        public static DependencyProperty ColorTrueProperty = DependencyProperty.Register("ColorTrue",
            typeof(Brush), typeof(BinView), new PropertyMetadata(Brushes.Yellow, ColorTruePropertyChanged));

        private static void ColorTruePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            (sender as BinView).OnColorTruePropertyChanged(sender, e);
        }

        private void OnColorTruePropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            _viewModel.UpdateColorTrue(e.NewValue as Brush);
        }

        public Brush ColorTrue
        {
            get { return (Brush)GetValue(ColorTrueProperty); }
            set
            {
                SetValue(ColorTrueProperty, value);
            }
        }

        #endregion

        #region ColorFalse

        public static DependencyProperty ColorFalseProperty = DependencyProperty.Register("ColorFalse",
            typeof(Brush), typeof(BinView), new PropertyMetadata(Brushes.Yellow, ColorFalsePropertyChanged));

        private static void ColorFalsePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            (sender as BinView).OnColorFalsePropertyChanged(sender, e);
        }

        private void OnColorFalsePropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            _viewModel.UpdateColorFalse(e.NewValue as Brush);
        }

        public Brush ColorFalse
        {
            get { return (Brush)GetValue(ColorFalseProperty); }
            set
            {
                SetValue(ColorFalseProperty, value);
            }
        }

        #endregion

        public BinView()
        {
            _viewModel = new BinViewModel();
            _viewModel.PropertyChanged += ViewModel_PropertyChanged;
            InitializeComponent();
            DataContext = _viewModel;
        }

        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "WordValue")
            {
                WordValue = _viewModel.WordValue;
                SendPropertyChanged("WordValue");
            }
        }

        void SendPropertyChanged(string property)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
