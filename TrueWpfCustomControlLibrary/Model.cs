﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWpfCustomControlLibrary
{
    public class WordBit
    {
        public bool IsOn { get; set; }
        public byte Number { get; set; }
    }
}
