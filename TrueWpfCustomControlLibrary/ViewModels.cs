﻿using GalaSoft.MvvmLight;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;

namespace TrueWpfCustomControlLibrary
{
    public class BinViewModel : ViewModelBase
    {
        private List<WordBit> _addressItems;
        private List<WordBit> _dataItems;
        private List<WordBitViewModel> _addressItemsVM;
        private List<WordBitViewModel> _dataItemsVM;

        public List<WordBitViewModel> AddressItems
        {
            get
            {
                return _addressItemsVM;
            }
        }
        public List<WordBitViewModel> DataItems
        {
            get
            {
                return _dataItemsVM;
            }
        }
        public int WordValue
        {
            get
            {
                int[] array = new int[1];
                var bitArray = new BitArray(_addressItems.Concat(_dataItems).Select(a => a.IsOn).ToArray());
                bitArray.CopyTo(array, 0);
                return array[0];
            }
            set
            {
                var bitArray = new BitArray((new[] { value }));
                var bits = bitArray.Cast<bool>().Select((o, i) => new { Value = o, Index = i }).Select(b => new WordBit { IsOn = b.Value, Number = (byte)b.Index }).ToList();
                _addressItems = bits.Where(b => b.Number < 8).ToList();
                if (_addressItemsVM != null && _addressItemsVM.Count > 0)
                {
                    _addressItemsVM.ForEach(a => a.PropertyChanged -= Bit_PropertyChanged);
                }
                _addressItemsVM = new List<WordBitViewModel>();
                foreach (var a in _addressItems)
                {
                    var bit = new WordBitViewModel(a);
                    bit.ColorTrue = ColorTrue;
                    bit.ColorFalse = ColorFalse;
                    bit.PropertyChanged += Bit_PropertyChanged;
                    _addressItemsVM.Add(bit);
                }
                _dataItems = bits.Where(b => b.Number >= 8).ToList();
                if (_dataItemsVM != null && _dataItemsVM.Count > 0)
                {
                    _dataItemsVM.ForEach(a => a.PropertyChanged -= Bit_PropertyChanged);
                }
                _dataItemsVM = new List<WordBitViewModel>();
                foreach (var a in _dataItems)
                {
                    var bit = new WordBitViewModel(a);
                    bit.ColorTrue = ColorTrue;
                    bit.ColorFalse = ColorFalse;
                    bit.PropertyChanged += Bit_PropertyChanged;
                    _dataItemsVM.Add(bit);
                }
                RaisePropertyChanged("AddressItems");
                RaisePropertyChanged("DataItems");
            }
        }
        public Brush ColorTrue { get; set; }
        public Brush ColorFalse { get; set; }

        public BinViewModel()
        {
            _dataItems = new List<WordBit>();
            _dataItemsVM = new List<WordBitViewModel>();
            _addressItems = new List<WordBit>();
            _addressItemsVM = new List<WordBitViewModel>();
            ColorTrue = Brushes.LightGray;
            ColorFalse = Brushes.Yellow;
            WordValue = 0;
        }

        public void UpdateColorTrue(Brush color)
        {
            ColorTrue = color;
            _addressItemsVM.ForEach(a => a.ColorTrue = color);
            _dataItemsVM.ForEach(a => a.ColorTrue = color);
        }
        public void UpdateColorFalse(Brush color)
        {
            ColorFalse = color;
            _addressItemsVM.ForEach(a => a.ColorFalse = color);
            _dataItemsVM.ForEach(a => a.ColorFalse = color);
        }

        private void Bit_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsOn")
            {
                RaisePropertyChanged("WordValue");
            }
        }
    }

    public class WordBitViewModel : ViewModelBase
    {
        private WordBit _model;

        public bool IsOn
        {
            get { return _model.IsOn; }
            set
            {
                _model.IsOn = value;
                RaisePropertyChanged("IsOn");
                RaisePropertyChanged("Background");
            }
        }
        public byte Number
        {
            get { return _model.Number; }
            set
            {
                _model.Number = value;
                RaisePropertyChanged("Number");
            }
        }
        public Brush Background
        {
            get
            {
                return IsOn ? ColorTrue : ColorFalse;
            }
        }
        public Brush ColorTrue { get; set; }
        public Brush ColorFalse { get; set; }

        public ICommand TurnOnOffCommand { get; set; }

        public WordBitViewModel(WordBit model)
        {
            _model = model;
            TurnOnOffCommand = new GalaSoft.MvvmLight.Command.RelayCommand(() =>
            {
                IsOn = !IsOn;
            });
        }
    }
}
